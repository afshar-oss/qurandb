
from flask_frozen import Freezer
from app import app

app.config['FREEZER_DESTINATION'] = 'public'
app.config['FREEZER_BASE_URL'] = 'https://afshar-oss.gitlab.io/qurandb/'

freezer = Freezer(app)

if __name__ == '__main__':
    for (url, path) in freezer.freeze_yield():
        print(url, path)
