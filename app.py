
# vim: ft=python sw=2 ts=2 sts=2 tw=80

import flask
import model

app = flask.Flask('quran')

class Page:
  def __init__(self, title, subtitle):
    self.title = title
    self.subtitle = subtitle

  @property
  def render_title(self):
    return f'Semantic Holy Quran - {self.title}'



@app.route('/')
def index():
  p = Page('Home', 'Welcome to the Semantic Holy Quran')
  return flask.render_template('index.html', page=p,
      chapters=model.get_chapters())

@app.route('/chapter<int:chapter_id>.html')
def chapter(chapter_id):
  ch = model.get_chapter(chapter_id)
  p = Page(ch.name, 'Read the surah')
  return flask.render_template('chapter.html', page=p, chapter=ch)

@app.route('/root<buckwalter>.html')
def root(buckwalter):
  r = model.get_root(buckwalter)
  p = Page(r.arabic, 'List of tokens with a root')
  return flask.render_template('root.html', root=r, page=p)

@app.route('/roots.html')
def roots():
  rs = model.get_roots()
  p = Page('List of roots', '')
  return flask.render_template('roots.html', roots=rs, page=p) 
  


if __name__ == '__main__':
  app.jinja_env.auto_reload = True
  app.config['TEMPLATES_AUTO_RELOAD'] = True
  app.run(debug=True)
