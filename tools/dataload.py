
# vim: ft=python sw=2 ts=2 sts=2 tw=80

import json
from model import *

def load_data():
  df = open('tools/quran_content.json')
  data = json.load(df)
  df.close()

  for cn, c in data['chapters'].items():
    ch = Chapter(number=cn, name=c['name'])
    session.add(ch)

    for vn, v in c['verses'].items():
      vs = Verse(number=vn)
      ch.verses.append(vs)

      for wn, w in v['words'].items():
        print(cn, vn, wn)

        #try:
        #  f = open(w['img'], 'rb')
        #except KeyError:
        #  print(w)
        #imgd = f.read()
        #f.close()
        
        wo = Word(number=wn,
            translation=w['translation'],
            transliteration=w['transliteration'],
            description=w['description'],
            #image=imgd
            )
        vs.words.append(wo)

        for tn, t in w['tokens'].items():
          to = Token(number=tn, word_id=wo.id,
              buckwalter=t['buckwalter'],
              simple=t['simple'],
              arabic=t['unicode'],
              plain_buckwalter=t['plain']['buckwalter'],
              plain_simple=t['plain']['simple'],
              plain_arabic=t['plain']['unicode'],
              tag=t['tag'],
              features='|'.join(t['features']))

          if t['root']:
            to.root_buckwalter = t['root']['buckwalter']
            to.root_simple = t['root']['simple']
            to.root_arabic = t['root']['unicode']

            eroot = session.query(Root).filter_by(
                buckwalter=t['root']['buckwalter']).first()
            if not eroot:
              eroot = Root(buckwalter=t['root']['buckwalter'],
                  arabic=t['root']['unicode'],
                  simple=t['root']['simple'])
              session.add(eroot)
            eroot.tokens.append(to)



          if t['lemma']:
            to.lemma_buckwalter = t['lemma']['buckwalter']
            to.lemma_simple = t['lemma']['simple']
            to.lemma_arabic = t['lemma']['unicode']
          wo.tokens.append(to)
        
        print("tokens:", len(wo.tokens))
      print("words:", len(vs.words))
    print("verses:", len(ch.verses))
    session.commit()



    
if __name__ == '__main__':
  load_data()
