
#! /usr/bin/jython

import sys
sys.path.append('tools/jqurantree-1.0.0.jar')

import os

fnpatt = "tmp/corpus.quran.com/wordmorphology.jsp?location=({}:{}:{}).html"
wimpat = "tmp/corpus.quran.com/wordimage?id={}"

from org.jqurantree.orthography import Token, Location, Document

f = open('tools/quranic-corpus-morphology-0.4.txt')


q = {}
q['chapters'] = {}

missing = []
total = []


bs = set()
for jc in Document.getChapters():
  bism = jc.getBismillah()
  if not bism:
      print(jc.getChapterNumber())


for line in f:
    if not line.startswith('('):
        continue
    parts = line.strip().split()

    if len(parts) == 3:
        parts.insert(1, ' ')
    if len(parts) == 5:
        parts[1] = parts[1]+' '+parts[2]
        del(parts[2])
    pos, form, tag, features = parts
    print([pos, form, tag, features])

    tar = Token.fromBuckwalter(form)
    s = tar.toSimpleEncoding()
    a = tar.toUnicode()
    print([s, a, form, tag])


    c,v,w,t = map(int, pos.strip('()').split(":"))
    print(c,v,w,t)





    if not c in q['chapters']:
        q['chapters'][c] = {'number': c, 'verses': {}}


    if not v in q['chapters'][c]['verses']:
        q['chapters'][c]['verses'][v] = {'number': v, 'words': {}}

    if not w in q['chapters'][c]['verses'][v]['words']:
        q['chapters'][c]['verses'][v]['words'][w] = {'number':w, 'tokens':{}}




        fn = fnpatt.format(c, v, w)

        total.append(fn)
        if not os.path.exists(fn):
            missing.append(fn)

        h = open(fn)
        nextl = False
        for fl in h:
            fl = fl.strip()
            #print(fl)

            if fl.startswith('<p class="first">'):
                desc = fl
                q['chapters'][c]['verses'][v]['words'][w]['description'] = desc
            elif fl.startswith('<h3 class="chapterName'):
                if len(q['chapters'][c]['verses']) == 1:
                    chname = fl.split('>')[1].split('<')[0]
                    q['chapters'][c]['name'] = chname

            elif fl.startswith('<h2>Verse'):
                wds = fl.split()
                pc, pv = map(int, wds[1].strip('(),').split(":"))
                pw = int(wds[3])
                print(pc, pv, pw)
                assert(pc == c)
                assert(pv == v)
                assert(pw == w)
                print(pc, pv, pw)
            elif fl.startswith('<a href="qurandictionary.jsp'):
                nextl = True
                transl = fl.split('>')[1].split('<')[0]
                print(transl)
                q['chapters'][c]['verses'][v]['words'][w]['transliteration'] = transl
            elif fl.startswith('<a href="https://corpus.quran.com/qurandictionary.jsp'):
                nextl = True
                transl = fl.split('>')[1].split('<')[0]
                print(transl)
                q['chapters'][c]['verses'][v]['words'][w]['transliteration'] = transl
            elif fl.startswith('<span class="phonetic"'):
                nextl = True
                transl = fl.split('>')[1].split('<')[0]
                q['chapters'][c]['verses'][v]['words'][w]['transliteration'] = transl

            elif nextl:
                trans = fl
                print(trans)
                q['chapters'][c]['verses'][v]['words'][w]['translation'] = trans
                nextl=False
            elif fl.startswith('src="wordimage') or fl.startswith('src="/wordimage"'):
                img = fl.split('"')[1].split('=')[1].lstrip('/')
                print(img)
                imgf = wimpat.format(img)
                if not os.path.exists(imgf):
                    missing.append(imgf)
                #imgfo = open(imgf, 'rb')
                #d = imgfo.read()
                q['chapters'][c]['verses'][v]['words'][w]['img'] = imgf
                #imgfo.close()
                break
            



    #if not t in q['chapters'][c]['verses'][v]['words'][w]['tokens']:
    #    q['chapters'][c]['verses'][v]['words'][w]['tokens'][t] = {}

    fs = features.split('|')

    tokend = {'number': t, 'buckwalter': form, 'simple': s, 'unicode': a, 'tag':tag, 'features': fs}

    tokend['root'] = None
    tokend['lemma'] = None
    tokend['case'] = None
    tokend['gender'] = None
    tokend['secondary'] = None
    for feat in fs:
        if feat.startswith('ROOT'):
            root = feat.split(':')[-1]
            roott = Token.fromBuckwalter(root)
            tokend['root'] = {'buckwalter': root, 'simple':
                    roott.toSimpleEncoding(), 'unicode': roott.toUnicode()} 
        elif feat.startswith('LEM'):
            lemma = feat.split(':')[-1]
            lemt = Token.fromBuckwalter(lemma)
            tokend['lemma'] = {'buckwalter': lemma, 'simple':
                    lemt.toSimpleEncoding(), 'unicode': lemt.toUnicode()}
        elif feat == 'STEM':
            tokend['stem'] = True
        elif feat == 'PREFIX':
            tokend['prefix'] = True
        elif feat == 'SUFFIX':
            tokend['suffix'] = True
        elif feat == 'GEN':
            tokend['case'] = 'genitive'
        elif feat == 'NOM':
            tokend['case'] = 'nominative'
        elif feat == 'ACC':
            tokend['case'] = 'accusative'
        elif feat == 'MS':
            tokend['gender'] = 'masculine singular'
        elif feat == 'FS':
            tokend['gender'] = 'feminine singular'
        elif feat == 'M':
            tokend['gender'] = 'masculine'
        elif feat == 'F':
            tokend['gender'] = 'feminine'
        elif feat == '2MP':
            tokend['gender'] = 'second person masculine plural'
        elif feat == '2MS':
            tokend['gender'] = 'second person masculine singular'
        elif feat == '3MP':
            tokend['gender'] = 'third person masculine plural'
        elif feat == '3MS':
            tokend['gender'] = 'third person masculine singular'
        elif feat == '1P':
            tokend['gender'] = 'first person'
        elif feat == 'MP':
            tokend['gender'] = 'masculine plural'
        elif feat == 'FP':
            tokend['gender'] = 'feminine plural'
        elif feat == '3FP':
            tokend['gender'] = 'third person feminine plural'
        elif feat == '3FS':
            tokend['gender'] = 'third person feminine singular'
        elif feat == '2FP':
            tokend['gender'] = 'second person feminine plural'
        elif feat == '2FS':
            tokend['gender'] = 'second person feminine singular'
        elif feat == 'PCPL':
            tokend['secondary'] = 'participle'
        elif feat == 'INDEF':
            tokend['secondary'] = 'indefinite'
        else:
            print('unused feature:', feat)



    plaint = tar.removeDiacritics()
    tokend['plain'] = {'buckwalter': plaint.toBuckwalter(), 'simple':
            plaint.toSimpleEncoding(), 'unicode': plaint.toUnicode()}


    q['chapters'][c]['verses'][v]['words'][w]['tokens'][t] = tokend


for c, ch in q['chapters'].items():
    if c != 9 and c!=1:
        q['chapters'][c]['verses'][0] = q['chapters'][1]['verses'][1]


import pprint
pprint.pprint(q['chapters'][1])

print (len(missing), len(total))
pprint.pprint(missing)

import json
f = open('tools/quran_content.json', 'w')
json.dump(q, f, indent=2)
f.close()
