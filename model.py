
# vim: ft=python sw=2 ts=2 sts=2 tw=80

import json


from sqlalchemy import create_engine
engine = create_engine('sqlite:///qdb.sqlite', echo=False)


from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, LargeBinary
from sqlalchemy import ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.orm import sessionmaker

Base = declarative_base()
Session = sessionmaker()
Session.configure(bind=engine)

class Chapter(Base):
  __tablename__ = 'chapters'
  id = Column(Integer, primary_key=True)
  number = Column(Integer)
  name = Column(String)

class Verse(Base):
  __tablename__ = 'verses'
  id = Column(Integer, primary_key=True)
  number = Column(Integer)
  chapter_id = Column(Integer, ForeignKey('chapters.id')) 
  chapter = relationship('Chapter', back_populates='verses')

class Word(Base):
  __tablename__ = 'words'
  id = Column(Integer, primary_key=True)
  number = Column(Integer)
  verse_id = Column(Integer, ForeignKey('verses.id'))
  verse = relationship('Verse', back_populates='words')

  translation = Column(String)
  transliteration = Column(String)
  description = Column(String)
  image = Column(LargeBinary)

class Root(Base):
  __tablename__ = 'roots'

  buckwalter = Column(String, primary_key=True)
  simple = Column(String)
  arabic = Column(String)

class Token(Base):
  __tablename__ = 'tokens'
  id = Column(Integer, primary_key=True)
  number = Column(Integer)
  word_id = Column(Integer, ForeignKey('words.id'))
  word = relationship('Word', back_populates='tokens')

  buckwalter = Column(String)
  simple = Column(String)
  arabic = Column(String)
  tag = Column(String)
  features = Column(String)
  
  root_id = Column(String, ForeignKey('roots.buckwalter'))
  root = relationship('Root', back_populates='tokens')

  root_buckwalter = Column(String)
  root_simple = Column(String)
  root_arabic = Column(String)
  
  plain_buckwalter = Column(String)
  plain_simple = Column(String)
  plain_arabic = Column(String)
  lemma_buckwalter = Column(String)
  lemma_simple = Column(String)
  lemma_arabic = Column(String)






Chapter.verses = relationship('Verse', order_by=Verse.number, back_populates='chapter')

Verse.words = relationship('Word', order_by=Word.number, back_populates='verse')

Word.tokens = relationship('Token', order_by=Token.number, back_populates='word')
Root.tokens = relationship('Token', order_by=Token.number, back_populates='root')

Base.metadata.create_all(engine)

session = Session()

def get_chapters():
  return session.query(Chapter).all()

def get_chapter(number):
  return session.query(Chapter).filter(Chapter.number==number).first()

def get_roots():
  return session.query(Root).all()

def get_root(buckwalter):
  return session.query(Root).filter(Root.buckwalter==buckwalter).first()
